/********************************************************************************************************
*
* UNIVERSITY OF COLORADO BOULDER
*
* @file kernel_module.c
* @brief kernel module for timer
* 
* This file initialises a timer using add_timer function.
* Timer is fired at every 500ms and writes the count of fires 
* to a file along with user given string
*
* @author Kiran Hegde
* @date  2/3/2018
* @tools vim editor
*
********************************************************************************************************/


/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/timer.h>
#include <linux/init.h>

/********************************************************************************************************
*
* Module description, licence and author information
*
********************************************************************************************************/

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Timer Example, Timer wakes upt at every 500ms");
MODULE_AUTHOR("KIRAN-HEGDE");

/*Function declaration for timer to call*/
void timer_callback(struct timer_list *timer);

int time_interval = 500;
struct timer_list my_timer;
static long int i=0;

/*Initialising string and number of times the timer should fire*/
static char *mystring = "blah";
static int no_times = 0;

/*Take input from the user about the number of times timer should fire 
and custom string*/

module_param(no_times, int, 0);

/*Description about the above parameter for kenrel module*/
MODULE_PARM_DESC(no_times, "Number of times timer should fire\n");
module_param(mystring, charp, 0000);
MODULE_PARM_DESC(mystring, "Name that has to bisplayed with count\n");

/********************************************************************************************************
*
* @name timer_module_entry()
* @brief initialisation for module
* 
* Once installed, kernel module starts executing from this function. 
* Timer is initialised in this function and time has been set to 500ms
*
* @param None
* @return return 0 on successful execution 
*
********************************************************************************************************/

static int __init timer_module_entry(void)
{
	printk(KERN_INFO "Initialising the timer module\n");
	timer_setup(&my_timer, timer_callback, 0);
	mod_timer(&my_timer, jiffies + msecs_to_jiffies(time_interval));
	return 0;
}

/********************************************************************************************************
*
* @name timer_module_exit()
* @brief remove/delete the timer
* 
* This function executes when the kernel module is removed.
* It removes the timer using del_timer function
*
* @param None
* @return None
*
********************************************************************************************************/
static void __exit timer_module_exit(void)
{
	del_timer(&my_timer);
	printk(KERN_INFO "Timer module deleted from kernel\n");
}


/*********************************************************************************************************
* @name timer_callback()
* @brief function is called everytime timer fires up
* 
* This function is called for every 500ms i.e is everytime timer fire up
* Logs the user given string and the count of timer firing up to the  
* kernel log. Once the count reaches its level, timer is deleted
*
* @param None
* @return return 0 on successful execution 
*
********************************************************************************************************/

void timer_callback(struct timer_list *timer)
{
        mod_timer(&my_timer, jiffies + msecs_to_jiffies(time_interval));
        printk(KERN_INFO "%s and count is %ld\n",mystring, i++);
        if(no_times==0)
        {
                del_timer(&my_timer);
		printk(KERN_INFO "Timer is deleted but timer module in the kernel still exist\n");
        }
        else
        {
                no_times--;
        }

}

/*Informing the Kernel about module entry function and
module exit function*/

module_init(timer_module_entry);
module_exit(timer_module_exit);
