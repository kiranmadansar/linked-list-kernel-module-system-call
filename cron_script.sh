#********************************************************************************************************
# UNIVERSITY OF COLORADO BOULDER
#
# @file cron_script.sh
# @brief script to execute a file
#
# This script executes the syscall_example.o object and 
# writes the output to the script.log file.
#
# @author Kiran Hegde
# @date 2/2/2018
#*******************************************************************************************************

#!/bin/bash	

/home/kiran/apes_hw2/./syscall_example.out >> /home/kiran/apes_hw2/script.log 2>&1
