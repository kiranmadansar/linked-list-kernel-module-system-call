Homework 2 Submission for APES

This repository contains the homework2 submission for APES subject at CU Boulder.

Mainly Contains System call implemetation, Timer using Kernel Module, Double Linked Data structure and Unit test for the same. 

cmocka_test.c contains all the details about how the test functions for the linked list is written and which all the boundary conditions are considered.

Inorder to run the cmocka unit testing for linked list, go into dlists directory using command

$ cd dlists

Then run the following command

$ gcc -o result.out cmocka_test.c dllists.c -lcmocka

Then execute the .out file using

$ ./result.out

Output of the unit test will be printed on the screen.
