/********************************************************************************************************
*
* UNIVERSITY OF COLORADO BOULDER
*
* @file syscall_example.c
* @brief invokes the custom created system call 
* 
* This file invokes several system call including the one custom created
* for sorting an array of numbers. Other system call includes getpid,
* getuid and gettime.
*
* @author Kiran Hegde
* @date  2/2/2018
*
********************************************************************************************************/


/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/

#include <stdio.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

/********************************************************************************************************
*
* @name main()
* @brief this function invokes several system call
*
* This function is the main function which invokes several function calls 
* including the ones which is custom defined for string sort. Other system 
* call includes getpid, getuid, gettime.
*
* @param None
* @return returns 0 on successful execution
*
********************************************************************************************************/

int main()
{
	/*Defining variables to get time*/
	time_t mytime;
	mytime=time(NULL);
	int32_t i=0, j=0;
	unsigned long n=256;
	int32_t temp;

	/*Dynamically allocating memory for storing n number of items*/
	int32_t * ptr = (int32_t *) malloc(n* sizeof(int32_t));
	int32_t * ptr_dest = (int32_t *) malloc(n* sizeof(int32_t));

	/*Check weather memory has been allocated*/
	if(!ptr || !ptr_dest)
	{
		printf("Memory intialization failed\n");
		exit(0);
	}
	srand(time(0));

	/*Generate n random numbers using rand()*/
	for(i=0; i<n; i++)
	{
		*(ptr+i)=rand();
	}
	
	/*Invoke the custom defined system call using its number*/
	int32_t res= syscall(333, ptr, ptr_dest, (unsigned long)(n));
	

	printf("\nBefore Sorting		After Sorting\n\n");
	for(i=0; i<n; i++)
	{
		printf("%d		%d\n",*(ptr+i), *(ptr_dest+i));
	}

	/*Free the allocated memory from heap*/
	free(ptr);
	free(ptr_dest);
	
	/*Print current process ID, User ID and time and date*/
	printf("\nCurrent Process ID is %d\n", getpid());
	printf("Current User ID is %d\n", getuid());
	printf("Current time is %s", ctime(&mytime));
	printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	return 0;
}
