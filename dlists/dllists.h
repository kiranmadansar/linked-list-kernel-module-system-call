/********************************************************************************************************
*
* @file dllists.h
* @brief contains structure definition and function declaration 
*  for doubly linked list 
*
* This file contains the structure definition and function declarations 
* needed for implementing double linked list
*
* @author Kiran Hegde
* @date 2/4/2018
* @tools vim editor
*
********************************************************************************************************/


#ifndef DLLISTS_H_

#define DLLISTS_H_

/********************************************************************************************************
*
* Header files
*
********************************************************************************************************/

#include <stdint.h>
#include <stdio.h>

/********************************************************************************************************
*
* Macros definition
*
********************************************************************************************************/

#define OFFSET_OF(type, member) ((size_t) &((type *)0)->member)

#define CONTAINER_OF(ptr, type, member) ({                      \
        const typeof ( ((type *)0)->member ) *__mptr = (ptr);           \
        (type *) ( (char *)__mptr - OFFSET_OF(type, member) );})

/********************************************************************************************************
*
* @name structure node_list
* @brief contains previous and next nodes info
*
* This structure defines the pointer to next and previous nodes 
* in the double linked list
*
* @param None
*
* @return None
*
********************************************************************************************************/

typedef struct node_list
{
	struct node_list *prev;
	struct node_list *next;
} node_list;

/********************************************************************************************************
*
* @name structure node_list
* @brief contains data and another struct
*
* This structure defines data that the node should contain 
* and also the structure that defines the pointer to next and previous nodes 
* in the double linked list
*
* @param None
*
* @return None
*
********************************************************************************************************/

typedef struct info
{
	unsigned long data;
	node_list Node;
} info_list;

/********************************************************************************************************
*
* @name destroy_linkedlist
* @brief destroys the linked list
*
* This function frees' all the allocated memories from heap 
* and destroys the doble linked list
*
* @param head pointer of the linked list
*
* @return returns NULL on successful execution
*
********************************************************************************************************/

node_list *destroy_linkedlist(node_list *head_node);

/********************************************************************************************************
*
* @name insert_at_beginning
* @brief inserts a node at the beginning
*
* This function inserts a node at the beginning of linked list
* If linked list doesn't exist then it creates one
*
* @param head_node: head pointer of the linked list
* @param new_node: new node that has to be added
*
* @return returns head pointer of the linked list  on successful execution
* 	  or NULL for any failure
*
********************************************************************************************************/

node_list *insert_at_beginning(node_list *head_node, node_list *new_node);

/********************************************************************************************************
*
* @name insert_at_end
* @brief inserts a node at the end
*
* This function inserts a node at the end of the linked list
* If linked list doesn't exist then it creates one
*
* @param head_node: head pointer of the linked list
* @param new_node: new node that has to be added
*
* @return returns head pointer of the linked list  on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

node_list *insert_at_end(node_list *head_node, node_list *new_node);

/********************************************************************************************************
*
* @name insert_at_position
* @brief inserts a node at the given position
*
* This function inserts a node at the given position of linked list
* If linked list doesn't exist then it creates one
*
* @param head_node: head pointer of the linked list
* @param new_node: new node that has to be added
* @param position: index after which the node has to be inserted
*
* @return returns head pointer of the linked list  on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

node_list *insert_at_position(node_list *head_node, node_list *new_node, unsigned long position);

/********************************************************************************************************
*
* @name delete_from_beginning
* @brief delete a node from the beginning
*
* This function deletes a node from the beginning of linked list
*
* @param head_node: head pointer of the linked list
*
* @return returns head pointer of the linked list  on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

node_list *delete_from_beginning(node_list *head_node);

/********************************************************************************************************
*
* @name delete_from_end
* @brief delete a node from the end
*
* This function deletes a node present at the end of the linked list
*
* @param head_node: head pointer of the linked list
*
* @return returns head pointer of the linked list  on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

node_list *delete_from_end(node_list *head_node);

/********************************************************************************************************
*
* @name delete_from_position
* @brief delete a node from the given position
*
* This function deletes a node from the given position of the linked list
*
* @param head_node: head pointer of the linked list
* @param position: index at which the node has to be deleted
*
* @return returns head pointer of the linked list  on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

node_list *delete_from_position(node_list *head_node, unsigned long position);

/********************************************************************************************************
*
* @name size_linkedlist
* @brief get the size of linked list
*
* This function gives the number of nodes present in the linked list
*
* @param head_node: pointer to ANY node of the linked list
*
* @return returns number of nodes present in the linked list on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

unsigned long size_linkedlist(node_list *any_node);

#endif
