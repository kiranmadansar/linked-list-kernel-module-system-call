/********************************************************************************************************
*
* @file cmocka_test.c
* @brief testing the written linked list using cmocka 
*
* This file tests the implemeted double linked list using cmocka for 
* different input parameters
*
* @author Kiran Hegde
* @date 2/4/2018
* @tools vim editor
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/

#include <stdio.h>
#include "dllists.h"
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

/* Declaration for structure pointer*/
info_list *ptr;
info_list *ptr1;

/********************************************************************************************************
*
* @name test_insert_at_beginning1
* @brief test the insert function
*
* This function tests the insert_at_beginning function 
* for input parameter of NULL head node
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_insert_at_beginning1(void **state)
{
	info_list * ptr = (info_list *)malloc(sizeof(info_list));
	node_list *result = insert_at_beginning(NULL, &(ptr->Node));
	assert_ptr_equal(result, &(ptr->Node));
	free(ptr);
}

/********************************************************************************************************
*
* @name test_insert_at_beginning2
* @brief test the insert function
*
* This function tests the insert_at_beginning function 
* for passing two valid pointers
*
* @param None
*
* @return None
*
********************************************************************************************************/
void test_insert_at_beginning2(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
	info_list * ptr1 = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_beginning(&(ptr1->Node), &(ptr->Node));
        assert_ptr_equal(result, &(ptr->Node));
        free(ptr);
	free(ptr1);
}

/********************************************************************************************************
*
* @name test_insert_at_beginning3
* @brief test the insert function
*
* This function tests the insert_at_beginning function 
* for input parameter of NULL new node
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_insert_at_beginning3(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_beginning(&(ptr->Node), NULL);
        assert_ptr_equal(result, NULL);
        free(ptr);
}

/********************************************************************************************************
*
* @name test_insert_at_end1
* @brief test the insert function
*
* This function tests the insert_at_end function 
* for input parameter of NULL head node
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_insert_at_end1(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_end(NULL, &(ptr->Node));
        assert_ptr_equal(result, &(ptr->Node));
        free(ptr);
}

/********************************************************************************************************
*
* @name test_insert_at_end2
* @brief test the insert function
*
* This function tests the insert_at_end function 
* for passing two valid pointers
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_insert_at_end2(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        info_list * ptr1 = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_end(&(ptr1->Node), &(ptr->Node));
        assert_ptr_equal(result, &(ptr1->Node));
        free(ptr);
        free(ptr1);
}

/********************************************************************************************************
*
* @name test_insert_at_end3
* @brief test the insert function
*
* This function tests the insert_at_end function 
* for input parameter of NULL new node
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_insert_at_end3(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_end(&(ptr->Node), NULL);
        assert_ptr_equal(result, NULL);
        free(ptr);
}

/********************************************************************************************************
*
* @name test_insert_at_position1
* @brief test the insert function
*
* This function tests the insert_at_position function 
* for input parameter of NULL head node
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_insert_at_position1(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_position(NULL, &(ptr->Node), 0);
        assert_ptr_equal(result, &(ptr->Node));
        free(ptr);
}

/********************************************************************************************************
*
* @name test_insert_at_position2
* @brief test the insert function
*
* This function tests the insert_at_position function 
* for valid input parameters
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_insert_at_position2(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        info_list * ptr1 = (info_list *)malloc(sizeof(info_list));
	info_list * ptr2 = (info_list *)malloc(sizeof(info_list));
	node_list *result = insert_at_beginning(NULL, &(ptr->Node));
	node_list *result1 = insert_at_end(result, &(ptr1->Node));
	node_list *result5 = insert_at_end(result1, &(ptr2->Node));
        node_list *result2 = insert_at_position(result5, &(ptr->Node), 1);
        assert_ptr_equal(result2, &(ptr->Node));
        free(ptr);
        free(ptr1);
	free(ptr2);
}

/********************************************************************************************************
*
* @name test_insert_at_position3
* @brief test the insert function
*
* This function tests the insert_at_position function 
* for invalid position
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_insert_at_position3(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
	info_list * ptr1 = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_position(&(ptr->Node), &(ptr1->Node),10);
        assert_ptr_equal(result, NULL);
        free(ptr);
	free(ptr1);
}

/********************************************************************************************************
*
* @name test_insert_at_position4
* @brief test the insert function
*
* This function tests the insert_at_position function 
* for input parameter of NULL new node
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_insert_at_position4(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_position(&(ptr->Node), NULL, 0);
        assert_ptr_equal(result, NULL);
        free(ptr);
}

/********************************************************************************************************
*
* @name test_delete_from_beginning
* @brief test the delete function
*
* This function tests the delete_from_beginning function 
* for valid input parameters
*
* @param None
*
* @return None
*
********************************************************************************************************/
void test_delete_from_beginning1(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_beginning(NULL, &(ptr->Node));
        info_list * ptr1 = (info_list *)malloc(sizeof(info_list));
        node_list *result1 = insert_at_end(result, &(ptr1->Node));
	node_list *result2 = delete_from_beginning(result1);
	assert_ptr_equal(result2, &(ptr1->Node));
	free(ptr1);
}

/********************************************************************************************************
*
* @name test_delete_from_beginning2
* @brief test the delete function
*
* This function tests the delete_from_beginning function 
* if only one node is present in linked list
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_delete_from_beginning2(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_beginning(NULL, &(ptr->Node));
	node_list *result1 = delete_from_beginning(result);
        assert_ptr_equal(result1, NULL);
}

/********************************************************************************************************
*
* @name test_delete_from_beginning3
* @brief test the delete function
*
* This function tests the delete_from_beginning function 
* for NULL pointer input parameter
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_delete_from_beginning3(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list *result = delete_from_beginning(NULL);
        assert_ptr_equal(result, NULL);
        free(ptr);
}

/********************************************************************************************************
*
* @name test_delete_from_end1
* @brief test the delete function
*
* This function tests the delete_from_end function 
* if only one node present in list
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_delete_from_end1(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_end(NULL, &(ptr->Node));
	node_list *result1 = delete_from_end(result);
        assert_ptr_equal(result1, NULL);
}

/********************************************************************************************************
*
* @name test_delete_from_end2
* @brief test the delete function
*
* This function tests the delete_from_end function 
* for valid parameter list
*
* @param None
*
* @return None
*
********************************************************************************************************/
void test_delete_from_end2(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        info_list * ptr1 = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_end(NULL, &(ptr->Node));
	node_list *result1 = insert_at_beginning(result, &(ptr1->Node));
     	node_list *result2 = delete_from_end(result1);
	assert_ptr_equal(result2, &(ptr1->Node));
        free(ptr1);
}

/********************************************************************************************************
*
* @name test_delete_from_end3
* @brief test the delete function
*
* This function tests the delete_from_end function 
* for passing NULL input pointer
*
* @param None
*
* @return None
*
********************************************************************************************************/
void test_delete_from_end3(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list *result = delete_from_end(NULL);
        assert_ptr_equal(result, NULL);
        free(ptr);
}

/********************************************************************************************************
*
* @name test_delete_from_position1
* @brief test the delete function
*
* This function tests the delete_from_position function 
* if only one position is present
*
* @param None
*
* @return None
*
********************************************************************************************************/
void test_delete_from_position1(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
	node_list *result = insert_at_beginning(NULL, &(ptr->Node));
        node_list *result1 = delete_from_position(result, 0);
        assert_ptr_equal(result1, NULL);
}

/********************************************************************************************************
*
* @name test_delete_from_position2
* @brief test the delete function
*
* This function tests the delete_from_position function 
* for valid input parameters
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_delete_from_position2(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        info_list * ptr1 = (info_list *)malloc(sizeof(info_list));
        info_list * ptr2 = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_beginning(NULL, &(ptr->Node));
        node_list *result1 = insert_at_end(result, &(ptr1->Node));
        node_list *result5 = insert_at_end(result1, &(ptr2->Node));
        node_list *result2 = delete_from_position(result5, 1);
        assert_ptr_equal(result2, &(ptr->Node));
        free(ptr);
        //free(ptr1);
        free(ptr2);
}

/********************************************************************************************************
*
* @name test_delete_from_position3
* @brief test the delete function
*
* This function tests the delete_from_position function 
* for invalid position 
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_delete_from_position3(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        info_list * ptr1 = (info_list *)malloc(sizeof(info_list));
        node_list *result = delete_from_position(&(ptr->Node),10);
        assert_ptr_equal(result, NULL);
        free(ptr);
        free(ptr1);
}

/********************************************************************************************************
*
* @name test_delete_from_position4
* @brief test the delete function
*
* This function tests the delete_from_position function 
* for passing NULL pointer
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_delete_from_position4(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list *result = delete_from_position(NULL, 0);
        assert_ptr_equal(result, NULL);
        free(ptr);
}

/********************************************************************************************************
*
* @name test_size_linkedlist1
* @brief test the size function
*
* This function tests the size_linkedlist function 
* for NULL input parameter
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_size_linkedlist1(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        unsigned long result = size_linkedlist(NULL);
        assert_int_equal(result, NULL);
        free(ptr);
}

/********************************************************************************************************
*
* @name test_size_linkedlist1
* @brief test the size function
*
* This function tests the size_linkedlist function 
* for valid input parameters
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_size_linkedlist2(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        info_list * ptr1 = (info_list *)malloc(sizeof(info_list));
        info_list * ptr2 = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_beginning(NULL, &(ptr->Node));
        node_list *result1 = insert_at_end(result, &(ptr1->Node));
        node_list *result5 = insert_at_end(result1, &(ptr2->Node));
        unsigned long result2 = size_linkedlist(result5);
        assert_int_equal(result2, 3);
        free(ptr);
        free(ptr1);
        free(ptr2);
}

/********************************************************************************************************
*
* @name test_destroy_linkedlist
* @brief test the destroy function
*
* This function tests the destroy_linkedlist function 
* for NULL input parameter
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_destroy_linkedlist(void **state)
{
	info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list * result = destroy_linkedlist(NULL);
        assert_int_equal(result, NULL);
        free(ptr);
}

/********************************************************************************************************
*
* @name test_destroy_linkedlist
* @brief test the destroy function
*
* This function tests the destroy_linkedlist function 
* for valid input parameters
*
* @param None
*
* @return None
*
********************************************************************************************************/

void test_destroy_linkedlist1(void **state)
{
        info_list * ptr = (info_list *)malloc(sizeof(info_list));
        node_list *result = insert_at_beginning(NULL, &(ptr->Node));
        node_list *result1 = destroy_linkedlist(result);
	assert_ptr_equal(result1, NULL);
}


/********************************************************************************************************
*
* @name main
* @brief all testing function called
*
* This function calls all the testing functions
* Also print the output to console
*
* @param None
*
* @return returns result of the test function
*
********************************************************************************************************/
int main()
{

	/*define a struct containing all the testing function*/
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_insert_at_beginning1),
		cmocka_unit_test(test_insert_at_beginning2),
		cmocka_unit_test(test_insert_at_beginning3),
		cmocka_unit_test(test_insert_at_end1),
                cmocka_unit_test(test_insert_at_end2),
                cmocka_unit_test(test_insert_at_end3),
		cmocka_unit_test(test_insert_at_position1),
		cmocka_unit_test(test_insert_at_position2),
		cmocka_unit_test(test_insert_at_position3),
		cmocka_unit_test(test_insert_at_position4),
		cmocka_unit_test(test_delete_from_beginning1),
                cmocka_unit_test(test_delete_from_beginning2),
                cmocka_unit_test(test_delete_from_beginning3),
		cmocka_unit_test(test_delete_from_end1),
                cmocka_unit_test(test_delete_from_end2),
                cmocka_unit_test(test_delete_from_end3),
		cmocka_unit_test(test_delete_from_position1),
		cmocka_unit_test(test_delete_from_position2),
		cmocka_unit_test(test_delete_from_position3),
		cmocka_unit_test(test_delete_from_position4),
		cmocka_unit_test(test_size_linkedlist1),
		cmocka_unit_test(test_size_linkedlist2),
		cmocka_unit_test(test_destroy_linkedlist),	
		cmocka_unit_test(test_destroy_linkedlist1),
	};

	return cmocka_run_group_tests(tests, NULL, NULL);
}
