/********************************************************************************************************
*
* UNIVERSITY OF COLORADO BOULDER
*
* @file dllists.c
* @brief implementation of double linked list
* 
* This file contains the necessary functions required for the 
* implementation of the double linked list
*
* @author Kiran Hegde
* @date  2/4/2018
* @tools vim editor
*
********************************************************************************************************/


/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "dllists.h"


/********************************************************************************************************
*
* @name destroy_linkedlist
* @brief destroys the linked list
*
* This function frees' all the allocated memories from heap 
* and destroys the doble linked list
*
* @param head pointer of the linked list
*
* @return returns NULL on successful execution
*
********************************************************************************************************/

node_list *destroy_linkedlist(node_list *head_node)
{
	/*check for null pointer parameter*/
	if(!head_node)
	{
		printf("Input parameter to destoy_linkedlist in NULL pointer\n");
		return NULL; 
	}
	
	node_list *temp_node;

	/*traverse every node forward and destroy the previous one*/
	while(head_node->next != NULL)
	{
		temp_node = head_node;
		head_node = head_node->next;
		head_node->prev = NULL; 
		info_list *ptr = CONTAINER_OF(temp_node, info_list, Node);
		free(ptr);
	}

	/* also destroy the last node of the linked list*/
	info_list *ptr = CONTAINER_OF(head_node, info_list, Node);
	free(ptr);
	return NULL;
}

/********************************************************************************************************
*
* @name insert_at_beginning
* @brief inserts a node at the beginning
*
* This function inserts a node at the beginning of linked list
* If linked list doesn't exist then it creates one
*
* @param head_node: head pointer of the linked list
* @param new_node: new node that has to be added
*
* @return returns head pointer of the linked list  on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

node_list *insert_at_beginning(node_list *head_node, node_list *new_node)
{
	/*check for null pointer*/
	if(!new_node)
	{
		printf("Input parameter to insert_at_beginning is NULL pointer\n");
		return NULL;
	}
	
	/*if linked list doesn't exist, create one*/
	if(!head_node)
	{
		new_node->next = NULL;
		new_node->prev = NULL;
		return new_node;
	}

	/*Otherwise add a node at the beginning of the list and make it head node*/
	new_node->next = head_node;
	head_node->prev = new_node;
	new_node->prev = NULL;
	return new_node;
}

/********************************************************************************************************
*
* @name insert_at_end
* @brief inserts a node at the end
*
* This function inserts a node at the end of the linked list
* If linked list doesn't exist then it creates one
*
* @param head_node: head pointer of the linked list
* @param new_node: new node that has to be added
*
* @return returns head pointer of the linked list  on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

node_list *insert_at_end(node_list *head_node, node_list *new_node)
{
	if(!new_node)
	{
		printf("Input parameter to insert_at_end is NULL pointer\n");
		return NULL;
	}
	node_list *temp_head = head_node;
	
	/*check if linked list exists, if not create one*/
	if(!head_node)
        {
                new_node->next = NULL;
                new_node->prev = NULL;
		return new_node;
        }

	/*traverse to the last node*/
	while(head_node->next != NULL)
	{
		head_node = head_node->next;
	}

	/*add a node at the end of the list*/
	head_node->next = new_node;
	new_node->prev = head_node;
	new_node->next = NULL;

	/*return the head node pointer*/
	return temp_head;
}

/********************************************************************************************************
*
* @name insert_at_position
* @brief inserts a node at the given position
*
* This function inserts a node at the given position of linked list
* If linked list doesn't exist then it creates one
*
* @param head_node: head pointer of the linked list
* @param new_node: new node that has to be added
* @param position: index after which the node has to be inserted
*
* @return returns head pointer of the linked list  on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

node_list *insert_at_position(node_list *head_node, node_list *new_node, unsigned long position)
{
	/*check if the inserting node is NULL*/
	if(!new_node)
	{
		printf("Input parameter to insert_at_position is NULL pointer\n");
                return NULL;
	}
	
	node_list *temp_head = head_node;
	
	/*If linked list doesnt exist, create one*/
	if(!head_node)
	{
		new_node->next = NULL;
		new_node->prev = NULL;
		return new_node;
	}

	/*check if inserting node has to be made head node*/
	if(!position)
	{
		new_node->next = head_node;
		new_node->prev = NULL;
		head_node->prev = new_node;
		return new_node;
	}

	/*Otherwise traverse for the given position*/
	while(position && head_node->next != NULL)
	{
		head_node = head_node->next;
		position--;
	}

	/*If given position exists, insert the node and return head node pointer*/
	if(head_node->next != NULL)
	{
		new_node->next = head_node->next;
		head_node->next->prev = new_node;
		head_node->next = new_node;
		new_node->prev = head_node;
		return temp_head;
	}
	else
	{
		/*If position doesnt exist, return NULL*/
		printf("Given position doesn't exist\n");
		return NULL;
	}
}

/********************************************************************************************************
*
* @name delete_from_beginning
* @brief delete a node from the beginning
*
* This function deletes a node from the beginning of linked list
*
* @param head_node: head pointer of the linked list
*
* @return returns head pointer of the linked list  on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

node_list *delete_from_beginning(node_list *head_node)
{
	/*check for null pointer*/
	if(!head_node)
	{
		printf("Input parameter to delete_from_beginning is NULL pointer\n");
		return NULL;
	}
	
	/*check if only one pointer exist, if yes, delete it*/
	if(head_node->next == NULL && head_node->prev == NULL)
	{
		info_list *ptr = CONTAINER_OF(head_node, info_list, Node);		
		free(ptr);
		return NULL;
	}	

	/*Otherwise, delete a node at the beginning and make next node as head node*/
	node_list *temp_head = head_node;
	head_node = head_node->next;
	head_node->prev = NULL;
	info_list *ptr = CONTAINER_OF(temp_head, info_list, Node);
	free(ptr);

	/*return head node*/
	return head_node;
}

/********************************************************************************************************
*
* @name delete_from_end
* @brief delete a node from the end
*
* This function deletes a node present at the end of the linked list
*
* @param head_node: head pointer of the linked list
*
* @return returns head pointer of the linked list  on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

node_list *delete_from_end(node_list *head_node)
{
	if(!head_node)
	{
		printf("Input parameter to delete_from_end is NULL pointer\n");
		return NULL;
	}
	
	/*check if only one node is present, if yes, delete it*/
	if(head_node->prev == NULL && head_node->next == NULL)
	{
		info_list *ptr = CONTAINER_OF(head_node, info_list, Node);
		free(ptr);
		return NULL;
	}
	/*Otherwise, traverse to the end*/
	node_list *temp_head = head_node;
	while(head_node->next != NULL)
	{
		head_node = head_node->next;
	}
	/*delete the end node and return the head node pointer*/
	node_list *del = head_node;
	head_node = head_node->prev;
	head_node->next = NULL;
	info_list *ptr = CONTAINER_OF(del, info_list, Node);
	free(ptr);
	return temp_head;
}

/********************************************************************************************************
*
* @name delete_from_position
* @brief delete a node from the given position
*
* This function deletes a node from the given position of the linked list
*
* @param head_node: head pointer of the linked list
* @param position: index at which the node has to be deleted
*
* @return returns head pointer of the linked list  on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

node_list *delete_from_position(node_list *head_node, unsigned long position)
{
	/*Check if head_node is NULL*/
	if(!head_node)
	{
		printf("Input parameter to delete_from_position is NULL pointer\n");
		return NULL;
	}
	
	node_list *temp_node = head_node;
	
	/*check if deleting position is zero and only one node is present, if yes
	  delete it*/
	if(!position && head_node->next==NULL && head_node->prev==NULL)
	{
		info_list *ptr=CONTAINER_OF(head_node, info_list, Node);
		free(ptr);
		return NULL;
	}
	
	/*check if deleting position is zero, if yes, delete it and make next node as head node*/
	if(!position)
	{
		head_node = head_node->next;
		head_node->prev = NULL;
		info_list *ptr = CONTAINER_OF(temp_node, info_list, Node); 
		free(ptr);
		return head_node;  		
	}
	
	/*traverse till the position, if exists*/
	while(position && head_node->next != NULL)
	{
		head_node = head_node->next;
		position--;
	}

	/*check if position exists or not*/
	if(head_node->next == NULL)
	{
		printf("Given position doesn't exist\n");
		return NULL;
	}
	/*If exists, delete the node and return the head node pointer*/
	node_list *head_n = head_node->next;
	node_list *head_p = head_node->prev;
	head_n->prev = head_node->prev;
	head_p->next = head_node->next;
	info_list *ptr = CONTAINER_OF(head_node, info_list, Node);
	free(ptr);	
	return temp_node;
}

/********************************************************************************************************
*
* @name size_linkedlist
* @brief get the size of linked list
*
* This function gives the number of nodes present in the linked list
*
* @param head_node: pointer to ANY node of the linked list
*
* @return returns number of nodes present in the linked list on successful execution
*         or NULL for any failure
*
********************************************************************************************************/

unsigned long size_linkedlist(node_list *any_node)
{
	/*check for null pointer*/
	if(!any_node)
	{
		printf("Input parameter to size_linkedlist is NULL pointer\n");
		return 0;
	}
	
	unsigned long count=0;
	
	node_list *temp_node = any_node;
	
	/*check the number of nodes present in backwards*/
	while(temp_node->prev != NULL)
	{
		temp_node = temp_node->prev;
		count++;
	}

	/*check the number of nodes present forwards*/
	while(any_node->next != NULL)
	{
		any_node = any_node->next;
		count++;
	}

	/*return the total count*/
	return (count+1);
}
