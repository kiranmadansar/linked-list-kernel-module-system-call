/*******************************************************************************************************
*
* UNIVERSITY OF COLORADO BOULDER
*
* @file_operations.c
* 
* This file contains some file operations including open, write and 
* close functions. Mainly used to perf * and strace evaluation of 
* system calls.
* 
* @author Kiran Hegde
*
* @date 01/29/2018
*
* @tools GCC compiler and VIM editor
*
********************************************************************************************************/


/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/

#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>

/*******************************************************************************************************
*
* Macro Definition
*
********************************************************************************************************/

#define FILE_NAME "example.txt"

/********************************************************************************************************
*
* @name main()
* @brief main function mainly dealing with file operations
*
* This main function prints some of the string to the console
* and handles some of the file opeartion such as open, close, 
* write and many more. 
*
* @param None
*
* @return integer 0
*
********************************************************************************************************/

int main()
{
	printf("Interesting String\n");

	/*Open a file in writing mode*/
	FILE* file_ptr = fopen(FILE_NAME, "w");
	
	/*Change the permission for file using chmod system call*/
	chmod("/home/kiran/apes_hw2/example", S_IRUSR | S_IWUSR);

	/*Write to file*/
	fprintf(file_ptr, "Wrote ");

	/*Close the file*/
	fclose(file_ptr);

	/*Open the file is append mode*/
	file_ptr = fopen(FILE_NAME, "a");

	/*Allocate some dynamic memory for receiving string*/
	char *ptr = (char *) malloc(10*sizeof(char));
	printf("Enter the string of maximum 10 characters\n");
	gets(ptr);

	/*Write the string to file*/
	fprintf(file_ptr, ptr);

	/*Flush file buffer*/
	fflush(file_ptr);
	fclose(file_ptr);
	file_ptr = fopen(FILE_NAME, "r");

	/*Get a character from file*/
	getc(file_ptr);

	/*Get string from file*/
	gets(file_ptr);
	fclose(file_ptr);

	/*Free the allocated memory from heap*/
	free(ptr);

	/*Return 0 on successful execution*/
	return 0;
}
