/********************************************************************************************************
*
* UNIVERSITY OF COLORADO BOULDER
*
* @file syscall_kernel.c
* @brief sorts the string
* 
* This file sorts the given string using kernel space. 
*
* @author Kiran Hegde
* @date  2/3/2018
* @tools vim editor
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/

#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <linux/uaccess.h>

/********************************************************************************************************
*
* @name sys_sortexample
* @brief sorts the given string
*
* This function sorts any inputted string using kernel space
*
* @param source pointer to the string to be sorted
* @param dest pointer to store the result after sorting
* @param n number to bytes to sort
*
* @return return 0 on successful execution or error numbers.
*
********************************************************************************************************/

asmlinkage long sys_sortexample( int32_t __user *source, int32_t __user *dest, unsigned long __user n)
{
	/*Log for user information*/
	printk(KERN_INFO "Entered sys_sortexample\n");
	
	/*check if number of elements to sort is zero*/
	if(n==0)
	{
		printk(KERN_INFO "Zero elements to copy\n");
		return -EFAULT;
	}
	uint32_t result;
	uint32_t i=0, j=0;
	int32_t temp;

	/*Dynamically allocate memory for storing the incoming items using kmalloc*/
	int32_t *presort =  (int32_t *)kmalloc((n*sizeof(int32_t)), GFP_USER);

	/*check if memory has been allocated*/
	if(!presort) 
	{
		printk(KERN_INFO "memory allocation failed\n");
		return -ENOMEM;
	}

	/*Copy the items from user space to kernel space*/
	result = copy_from_user(presort, source, sizeof(int32_t)*n);
	
	/*check if all the elements have been copied*/
	if(result!=0)
	{
		kfree(presort);
		printk(KERN_INFO "copy from user failed\n ");
		
		/*return fault number to indicate that the systemcall din't execute successfully*/
		return -EFAULT;
	}

	/*bubblesort algorithm*/
	for(i=0; i<n; i++)
	{
		for(j=i+1; j<n; j++)
		{
			/*check if a number is greater and swap accordingly*/
			if(*(presort+i) < *(presort+j))
			{
				temp=*(presort+i);
				*(presort+i)=*(presort+j);
				*(presort+j)=temp;
			}
		}
	}

	/*Copy back the sorted elements to user space from kernel space*/
	result = copy_to_user(dest, presort, sizeof(int32_t)*n);


	if(result!=0) 
	{
		kfree(presort);
		printk(KERN_INFO "copy to user failed\n ");

		/*return fault number to indicate that the systemcall din't execute successfully*/
		return -EFAULT;
	}

	/*Free the dynamically allocated memory using kfree*/
	kfree(presort);
	printk(KERN_INFO "Sorting is done\n");

	/*return zero on successful execution*/
	return 0;
}
