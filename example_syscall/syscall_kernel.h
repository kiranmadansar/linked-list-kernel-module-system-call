/********************************************************************************************************
*
* @file syscall_kenrel.h
* @brief header file for the syscall_kernel.c
*
* This file contains all the function declaration for
* syscall_kernel.c file which is used for sorting 
* a string in kernel space
*
* @author Kiran Hegde
* @date 2/3/2018
* @tools vim editor
*
********************************************************************************************************/

#ifndef SYS_SORTEXAMPLE_H_

#define SYS_SORTEXAMPLE_H_

/********************************************************************************************************
*
* @name sys_sortexample
* @brief sorts the given string
*
* This function sorts any inputted string using kernel space
*
* @param source pointer to the string to be sorted
* @param dest pointer to store the result after sorting
* @param n number to bytes to sort
*
* @return return 0 on successful execution or error numbers.
*
********************************************************************************************************/ 

asmlinkage long sys_sortexample(int32_t __user *source, int32_t __user *dest, unsigned long __user n);

#endif
